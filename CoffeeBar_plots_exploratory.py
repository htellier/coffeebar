import numpy as np
import matplotlib.pyplot as plt
import CoffeeBar_Part1


#Plot's creation
muffin = 0
pie = 0
cookie = 0
sandwich = 0

for ele in CoffeeBar_Part1.list_food:
    if (ele == 'muffin'):
        muffin += 1
    elif (ele == 'cookie'):
        cookie += 1
    elif (ele == 'sandwich'):
        sandwich += 1
    elif (ele == 'pie'):
        pie += 1

n_groups = 4

amount_food = (muffin, sandwich, pie, cookie)
std_food = (0, 0, 0, 0)


fig, ax = plt.subplots()

index = np.arange(n_groups)
bar_width = 0.35

opacity = 1
error_config = {'ecolor': '0.3'}

rects1 = plt.bar(index, amount_food, bar_width,
                 alpha=opacity,
                 color='y',
                 yerr=std_food,
                 error_kw=error_config,
                 label='Food')


plt.xlabel('Food')
plt.ylabel('Amount')
plt.title('Amount of food')
plt.xticks(index + bar_width / 999999999999999, ('Muffin', 'Sandwich', 'Pie', 'Cookie'))
plt.legend()
plt.tight_layout()
plt.show()

#drink

soda = 0
tea = 0
coffee = 0
water = 0
frappucino = 0
milkshake = 0

for ele in CoffeeBar_Part1.list_drink:
    if (ele == 'soda'):
        soda += 1
    elif (ele == 'tea'):
        tea += 1
    elif (ele == 'coffee'):
        coffee += 1
    elif (ele == 'water'):
        water += 1
    elif (ele == 'frappucino'):
        frappucino += 1
    elif (ele == 'milkshake'):
        milkshake += 1

n_groups = 6

amount_drink = (soda, tea, coffee, water, frappucino, milkshake)
std_drink = (0, 0, 0, 0, 0, 0)


fig, ax = plt.subplots()

index = np.arange(n_groups)
bar_width = 0.35

opacity = 1
error_config = {'ecolor': '0.3'}

rects1 = plt.bar(index, amount_drink, bar_width,
                 alpha=opacity,
                 color='g',
                 yerr=std_drink,
                 error_kw=error_config,
                 label='Food')


plt.xlabel('Drink')
plt.ylabel('Amount')
plt.title('Amount of drink')
plt.xticks(index + bar_width / 999999999999999, ('soda', 'tea', 'coffee', 'water', 'frappucino', 'milkshake'))
plt.legend()
plt.tight_layout()
plt.show()