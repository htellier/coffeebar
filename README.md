# Exam Project

Exam Project as part of the data analytics course.

## Getting Started

In order to run the project, download the code and run it on PyCharm

### Prerequisites

You have to install Pycharm with the Python 3.6 version, anaconda, pandas and numpy

## Running the tests

Run the "CoffeeBar_Part1" first. In order to print the answer, delete quotation marks lines 159, 205, 210 and 217
After that, run "CoffeeBar_plots_exploratory" and check on your desk the plot. !!!Attention!!! the second plot come after closing the first one
Last step is to run CoffeeBar_Part2-3 and wait +/- 15 minutes. A csv file will be opened.
You can also print some elements of the different classes.

